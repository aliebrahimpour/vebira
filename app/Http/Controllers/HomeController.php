<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Models\Sound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $setting = Setting::first();
        return view('home', compact('setting'));
    }




    public function search()
    {
        $keyword = request('search');

        $sounds = Sound::search($keyword)->get();
        return view('product.find',compact('sounds'));
//        return $sounds;
    }

    public function staticPages()
    {
        $url = request()->getRequestUri();

        $url = str_replace('/', '', $url);
        $contents = DB::table('static_pages')->where('link', '=', $url)->get('description');
        if ($contents->isEmpty()) return abort(404);
        return view('staticPage', compact('contents'));
    }

    public function product()
    {
        return view('product.index');
    }

    public function card()
    {
        return view('product.card');
    }

}

<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Sound;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PanelController extends Controller
{
    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        return view('panel.index');
    }
    /**
     * Show the User dashboard.
     *
     */
    public function UserIndex()
    {
        return view('userProfile.index');
    }
    public function UserProfile()
    {
        return view('userProfile.profile');
    }
    public function UserUpload()
    {
        return view('userProfile.upload');
    }

    public function UserBuy()
    {
        $payments = Payment::where('user_id','=',auth()->user()->id)->where('sucsess','=',1)->get();
        return view('userProfile.buy',compact('payments'));
    }

    public function updateProfile(Request $request)
    {
        $user = User::whereId($request->user_id)->first();
        $this->validate($request, [
            'mobile' => 'max:11|min:11',
        ]);

        if ($request->has('role')) {
            $role = $request->role;
        }
        else{
            $role = 'user';
        }

        $userUpdate = $user->update([
            'name' => $request->userName,
            'role' => $role,
            'enabled' => $request->enabled,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'sex' => $request->sex,
            'address' => $request->address,
        ]);

        alert()->success('اطلاعات با موفقیت ثبت شد', ' ثبت اطلاعات')->autoclose(3500)->persistent('بستن');
        return redirect(route('user-panel.profile'));
    }
    public function UserUploadpodcast(Request $request)
    {
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $podcastPath = "/upload/user/podcast/{$year}/{$month}/{$day}/";
        $imagePath = "/upload/user/images/{$year}/{$month}/{$day}/";


        //podcast
        $podcast = $request->file('podcast');
        $filename = $podcast->getClientOriginalName();
        $podcast = $podcast->move(public_path($podcastPath), $filename);
        $inputs['podcast'] = $podcastPath . $filename;

        $picture = $request->file('picture');
        $pictureName = $picture->getClientOriginalName();
        $picture = $picture->move(public_path($imagePath), $pictureName);
        $inputs['picture'] = $imagePath . $pictureName;


        $sound = Sound::create([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'picture' => $inputs['picture'],
            'podcast' => $inputs['podcast'],
            'speakers' => $request->speakers,
            'type' =>'0',   //0 free 1 cost
            'price' => '0',
            'user_id' => Auth::user()->id,
        ]);
        return redirect(route('user-panel.index'));
    }

    public function UserDeletepodcast(Request $request){
        $sound =  Sound::whereId($request->sound_id)->first();
        $sound->delete();
        return redirect(route('user-panel.index'));

    }


}

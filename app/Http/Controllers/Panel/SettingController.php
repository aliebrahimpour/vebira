<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Models\Footer;
use App\Models\Menu;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function menu()
    {
        $menus = Menu::paginate(10);
//        return $menus;
        return view('panel.menu.all', compact('menus'));
    }

    public function addToMenu(Request $request)
    {
        $servant = Menu::updateOrCreate([
            'page_id' => $request->page_id,
        ]);
        return redirect()->back();
    }

    public function removeToMenu(Menu $menu)
    {
        $menu->delete();
        return redirect()->back();
    }


//    public function footer()
//    {
//        $footer = Footer::first();
//        return view('panel.footer.create',compact('footer'));
//    }
//
//    public function addFooter(Request $request)
//    {
//        $footer = Footer::first();
//        $footer->update([
//            'description' => $request->description,
//        ]);
//        return redirect()->back();
//    }

    public function slider()
    {
        $sliders = Slider::paginate(10);
        return view('panel.slider.all', compact('sliders'));
    }


    public function publicSetting()
    {
        $setting = Setting::first();
        return view('panel.setting.all', compact('setting'));
    }

    public function updateSetting(Request $request)
    {
//        $this->validate($request,[
//            'linkVideo1' => 'required',
//            'linkVideo2' => 'required',
//            'linkVideo3' => 'required',
//        ]);
        $setting = Setting::firstOrFail();

        $inputs = $request->except(['_token', '_method']);
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $podcastPath = "/upload/video/{$year}/{$month}/{$day}/";
        $imagePath = "/upload/video/{$year}/{$month}/{$day}/";

        //linkVideo1
//        if ($request->hasFile('linkVideo1')) {
//            $podcast = $request->file('linkVideo1');
//            $filename = $podcast->getClientOriginalName();
//            $podcast = $podcast->move(public_path($podcastPath), $filename);
//            $inputs['linkVideo1'] = $podcastPath . $filename;
//        } else {
//            $inputs['linkVideo1'] = $setting->linkVideo1;
//        }
//
//        //linkVideo2
//        if ($request->hasFile('linkVideo2')) {
//            $podcast = $request->file('linkVideo2');
//            $filename = $podcast->getClientOriginalName();
//            $podcast = $podcast->move(public_path($podcastPath), $filename);
//            $inputs['linkVideo2'] = $podcastPath . $filename;
//        } else {
//            $inputs['linkVideo2'] = $setting->linkVideo1;
//        }
//
//        //linkVideo3
//        if ($request->hasFile('linkVideo3')) {
//            $podcast = $request->file('linkVideo3');
//            $filename = $podcast->getClientOriginalName();
//            $podcast = $podcast->move(public_path($podcastPath), $filename);
//            $inputs['linkVideo3'] = $podcastPath . $filename;
//        } else {
//            $inputs['linkVideo3'] = $setting->linkVideo1;
//        }

//        $setting = Setting::first();
//        return $request;
        $setting->update($inputs);
        return redirect()->back();
    }

}

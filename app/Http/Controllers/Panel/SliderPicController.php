<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\SliderPic;
use Illuminate\Http\Request;

class SliderPicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SliderPic  $sliderPic
     * @return \Illuminate\Http\Response
     */
    public function show(SliderPic $sliderPic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SliderPic  $sliderPic
     * @return \Illuminate\Http\Response
     */
    public function edit(SliderPic $sliderPic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SliderPic  $sliderPic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SliderPic $sliderPic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SliderPic  $sliderPic
     * @return \Illuminate\Http\Response
     */
    public function destroy(SliderPic $sliderPic)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Panel;
use App\Http\Controllers\Controller;

use App\Models\SoundCategory;
use Illuminate\Http\Request;

class SoundCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SoundCategories = SoundCategory::paginate(10);
        return view('panel.soundCategories.all',compact('SoundCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.soundCategories.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
        ]);
        SoundCategory::create($request->all());
        return redirect(route('panelCategory.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SoundCategory  $soundCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SoundCategory $soundCategory)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SoundCategory  $soundCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $soundCategory = SoundCategory::whereId($id)->first();
        return view('panel.soundCategories.edit',compact('soundCategory'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\SoundCategory $soundCategory
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string',
        ]);
        $soundCategory = SoundCategory::whereId($id)->first();
        $soundCategory->update($request->all());
        return redirect(route('panelCategory.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SoundCategory  $soundCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $soundCategory = SoundCategory::whereId($id)->first();
        $soundCategory->delete();
        return redirect(route('panelCategory.index'));

    }
}

<?php

namespace App\Http\Controllers\Panel;
use App\Http\Controllers\Controller;

use App\Models\Sound;
use App\Models\SoundCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SoundController extends Controller
{
    /**
     * accept comment in panel by admin
     **/
    public function accept($id)
    {
        $comment = Sound::where('id', $id)->first();
        $comment->update(['enabled' => 1]);
        return redirect()->back();
    }

    /**
     * reject comment in panel by admin
     **/
    public function unAccept($id)
    {
        $comment = Sound::where('id', $id)->first();
        $comment->update(['enabled' => 0]);
        return redirect()->back();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sounds = Sound::paginate(10);
        return view('panel.sounds.all',compact('sounds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.sounds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request() , [
            'picture' => 'required|mimes:jpeg,png,bmp,jpg',
        ]);

        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $podcastPath = "/upload/podcast/{$year}/{$month}/{$day}/";
        $imagePath = "/upload/images/{$year}/{$month}/{$day}/";


        //podcast
        $podcast = $request->file('podcast');
        $filename = $podcast->getClientOriginalName();
        $podcast = $podcast->move(public_path($podcastPath), $filename);
        $inputs['podcast'] = $podcastPath . $filename;

        $picture = $request->file('picture');
        $pictureName = $picture->getClientOriginalName();
        $picture = $picture->move(public_path($imagePath), $pictureName);
        $inputs['picture'] = $imagePath . $pictureName;


        $sound = Sound::create([
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'picture' => $inputs['picture'],
            'podcast' => $inputs['podcast'],
            'speakers' => $request->speakers,
            'type' => $request->type,   //0 free 1 cost
            'price' => $request->price,
            'user_id' => Auth::user()->id,
        ]);


        return redirect(route('podcast.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sound  $sound
     * @return \Illuminate\Http\Response
     */
    public function show(Sound $sound)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sound  $sound
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sound =  Sound::whereId($id)->first();
        return view('panel.sounds.edit', compact('sound'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sound  $sound
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sound $sound)
    {
        $this->validate(request() , [
            'picture' => 'required|mimes:jpeg,png,bmp,jpg',
            
        ]);

        $inputs = $request->except(['_token', '_method','sound_id']);
        $year = Carbon::now()->year;
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $podcastPath = "/upload/podcast/{$year}/{$month}/{$day}/";
        $imagePath = "/upload/images/{$year}/{$month}/{$day}/";

        //podcast
        $podcast = $request->file('podcast');
        $filename = $podcast->getClientOriginalName();
        $podcast = $podcast->move(public_path($podcastPath), $filename);
        $inputs['podcast'] = $podcastPath . $filename;

        $picture = $request->file('picture');
        $pictureName = $picture->getClientOriginalName();
        $picture = $picture->move(public_path($imagePath), $pictureName);
        $inputs['picture'] = $imagePath . $pictureName;
        $inputs['user_id'] =Auth::user()->id;

        $sound = Sound::whereId($request->sound_id)->update($inputs);
        return redirect(route('podcast.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sound  $sound
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sound =  Sound::whereId($id)->first();
        $sound->delete();
        return redirect(route('podcast.index'));
    }

    public function uploadImageSubject()
    {
        $this->validate(request() , [
            'upload' => 'required|mimes:jpeg,png,bmp,jpg',
        ]);

        $year = Carbon::now()->year;
        $imagePath = "/upload/images/{$year}/";

        $file = request()->file('upload');
        $filename = $file->getClientOriginalName();

        if(file_exists(public_path($imagePath) . $filename)) {
            $filename = Carbon::now()->timestamp . $filename;
        }

        $file->move(public_path($imagePath) , $filename);
        $url = $imagePath . $filename;

        return "<script>window.parent.CKEDITOR.tools.callFunction(1 , '{$url}' , '')</script>";
    }


}

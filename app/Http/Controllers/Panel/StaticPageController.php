<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;

use App\Models\StaticPage;
use Illuminate\Http\Request;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = StaticPage::paginate(10);
        return view('panel.StaticPages.all', compact('pages'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.StaticPages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = StaticPage::create([
            'name' => $request->name,
            'title' => $request->title,
            'link' => $request->link,
            'description' => $request->description,
            'tags' => $request->tags,
        ]);
//        return redirect()->back();
        return redirect(route('static-page.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StaticPage $staticPage
     * @return void
     */
    public function show(StaticPage $staticPage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StaticPage $staticPage
     * @return \Illuminate\Http\Response
     */
    public function edit(StaticPage $staticPage)
    {
        $page = $staticPage;
        //        $page = StaticPage::whereId($id)->first();
        return view('panel.StaticPages.edit', compact('page'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\StaticPage $staticPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StaticPage $staticPage)
    {
//        $page = Page::whereId($id)->first();
        $staticPage->update($request->all());
        return redirect(route('static-page.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StaticPage $staticPage
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(StaticPage $staticPage)
    {
        $staticPage->delete();
        return redirect(route('static-page.index'));
    }
}

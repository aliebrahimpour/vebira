<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use function PHPSTORM_META\type;
use \SoapClient;

class PaymentController extends Controller{

//    private static $p=0;

    public function payment()
    {
        $record = new Payment();
        $record->user_id = auth()->user()->id;
        $record->code = implode(',',array_keys(session::get('cart')));
        $code=$record->code ;



        $price = request('total_price');
        session::put('price', $price);

        $MerchantID = 'ddc717ce-3ebb-49a3-8980-c36f9230ceda'; //Required
//        $Description = $resquest->description; // Required
//        $Email = 'UserEmail@Mail.Com'; // Optional
//        $Mobile = '09156583780'; // Optional
        $CallbackURL = 'https://jahaneasar.com/product/callback'; // Required

        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [
                'MerchantID' => $MerchantID,
                'Amount' => $price,
                'Description' => 'buy from aliebrahimpour',
                'CallbackURL' => $CallbackURL,
            ]
        );
        $record->authority = $result->Authority;
        $record->save();

//Redirect to URL You can do it also by creating a form
        if ($result->Status == 100) {
            return redirect('https://www.zarinpal.com/pg/StartPay/' . $result->Authority);
//برای استفاده از زرین گیت باید ادرس به صورت زیر تغییر کند:
//Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority.'/ZarinGate');
        } else {
            echo 'ERR: ' . $result->Status;
        }
    }

    public function callback()
    {

        $MerchantID = 'ddc717ce-3ebb-49a3-8980-c36f9230ceda';
        $Authority = $_GET['Authority'];
        $Amount = session::get('price');



        if ($_GET['Status'] == 'OK') {
            $record = Payment::where('user_id',auth()->user()->id)->firstOrFail();

            $record= DB::table('payments')->where('authority',$Authority)->update(['sucsess'=>1]);
            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $Amount,
                ]
            );

            if ($result->Status == 100) {
                session::forget('cart');
                return redirect('user-panel/buy');
            } else {
                echo 'Transaction failed. Status:' . $result->Status;
            }
        } else {
            echo 'Transaction canceled by user';
            return view('product.fail');
        }

    }
}

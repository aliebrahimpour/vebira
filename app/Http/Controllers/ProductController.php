<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Sound;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podcast = request('podcast_id');
//        $Products = Product::paginate(10);
        $Product = Sound::whereId($podcast)->first();
        $comments = $Product->comments->where('status', '=', '1');
        $exist = 0;
        if (session::has('cart')) {
            $cart = session::get('cart');
            if (array_key_exists($Product->id, $cart)) {
                $exist = 1;
            }
        }

//        return session()->get('cart');
        return view('product.index', compact('Product', 'comments', 'exist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function addtocard()
    {
        $podcast = request('podcast_id');
        if (session::has('cart')) {
            $cart = session::get('cart');
            if (array_key_exists($podcast, $cart)) {
                $cart[$podcast]++;
            } else {
                $cart[$podcast] = 1;
            }

        } else {
            $cart = array();
            $cart[$podcast] = 1;
        }

        session::put('cart', $cart);
        return redirect()->back();
    }

    public function removetocard()
    {
        $podcast = request('podcast_id');
        if (session::has('cart')) {
            $cart = session::get('cart');
            if (array_key_exists($podcast, $cart)) {
                unset($cart[$podcast]);
            }
        }
        session::put('cart', $cart);
        return redirect()->back();
    }


}

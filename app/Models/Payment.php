<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable=['authority','success'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'name','name2', 'linkVideo1', 'linkVideo2', 'linkVideo3', 'content1', 'content2','content3','label'
    ];
}

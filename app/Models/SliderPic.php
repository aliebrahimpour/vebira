<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderPic extends Model
{
    protected $fillable=['address','slider_id'];
}

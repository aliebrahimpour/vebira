<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Sound extends Model
{
//    use Searchable;

    public function scopeSearch($query, $keywords)
    {
        $keywords = explode(' ', $keywords);
        foreach ($keywords as $keyword) {
            $query->Where('title', 'LIKE', '%' . $keyword . '%')
                ->orWhere('speakers', 'LIKE', '%' . $keyword . '%');
        }
        return $query;
    }

    protected $fillable = ['title', 'description', 'enabled', 'picture', 'podcast',
        'speakers', 'type', 'price', 'category_id','user_id'];

    public function soundCategory()
    {
        return $this->belongsToMany(SoundCategory::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }


//    public function toSearchableArray()
//    {
//        return [
//            'title' => $this->title,
//            'description' => $this->description,
//            'create_at' => $this->created_at,
//            'speakers' => $this->speakers
//        ];
//    }
}

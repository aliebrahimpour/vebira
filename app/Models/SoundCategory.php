<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoundCategory extends Model
{
    protected $fillable=['name','description'];
    protected $guarded = [];

    public function sound()
    {
        return $this->belongsToMany(Sound::class);
    }
}

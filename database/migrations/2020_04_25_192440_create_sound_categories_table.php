<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoundCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sound_categories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
//            $table->string('icon')->nullable();
            $table->string('description')->nullable();

//            $table->unsignedBigInteger('sound_id')->unsigned()->nullable();
//            $table->foreign('sound_id')->references('id')->on('sounds')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sound_categories');
    }
}

@extends('layouts.app')

@section('content')
    <main>
        <div class="container my-4">
            <div class="card w-75 mx-auto">
                <div class="card-body">
                    @include('layouts.errors')
                    <div class="container">
                        <div class="row">
                            <div class="login-header mx-auto">
                                <a id="login-btn" class="login-header-btn active" href="#">ورود</a>
                                <a id="register-btn" class="login-header-btn" href="#">ثبت نام</a>
                            </div>
                        </div>
                        <form id="login-form" class="login-form text-right mt-4" data-visible="true" method="POST"
                              action="{{ route('login') }}">
                            @csrf

                            <div class="form-group">
                                <label for="email">ایمیل را وارد کنید</label>
                                <input type="email" name="email" class="form-control" id="email"
                                       aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="password">رمز عبور را وارد کنید</label>
                                <input type="password" name="password" class="form-control" id="password">
                            </div>
                            {{--<div class="form-group">--}}
                            {{--<a class="forget-pass" href="#">رمز عبور خود را فراموش کردید؟</a>--}}
                            {{--</div>--}}
                            <div class="form-group">
                                <input class="login-btn" type="submit" value="ورود">
                                {{--<a href="#" class="login-google">--}}
                                {{--<img width="30" src="../../assets/img/google-icon.png" alt="">--}}
                                {{--<span>ورود با گوگل</span>--}}
                                {{--</a>--}}
                            </div>
                        </form>
                        <form method="POST" action="{{ route('register') }}" id="register-form" class="login-form text-right mt-4" data-visible="false">
                            @csrf

                            <div class="form-group">
                                <label for="reg-email">ایمیل را وارد کنید</label>
                                <input type="email" name="email" class="form-control" id="reg-email"
                                       aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="reg-password">رمز عبور را وارد کنید</label>
                                <input type="password" name="password" class="form-control" id="reg-password">
                            </div>
                            <div class="form-group">
                                <label for="reg-password-confirm">تکرار رمز عبور را وارد کنید</label>
                                <input type="password" name="password_confirmation" class="form-control"
                                       id="reg-password-confirm">
                            </div>
                            <div class="form-group form-check">
                                <label for="agree" class="agree-label">
                                    <input type="checkbox" class="form-check-input" id="agree">
                                    <span class="check-agree"></span>
                                    {{--<a class="" href="#">--}}
                                        {{--شرایط و قوانین استفاده از سرویس های شنوتو--}}
                                    {{--</a>--}}
                                    {{--<span class="col-4e">--}}
                                        {{--را مطالعه نموده و با آن موافقم.--}}
                                    {{--</span>--}}
                                </label>
                            </div>
                            <div class="form-group">
                                <input class="register-btn" type="submit" value="ثبت نام">
                                {{--<a href="#" class="login-google">--}}
                                    {{--<img width="30" src="../../assets/img/google-icon.png" alt="">--}}
                                    {{--<span>ورود با گوگل</span>--}}
                                {{--</a>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection


@section('js')
    <script>
      $(document).ready(function () {
        $("#register-form").fadeOut(0);

        $("#login-form input").keyup(function () {
          if ($("#email").val() != "" && $("#password").val() != "") {
            $(".login-btn").addClass("true-log");
          } else {
            $(".login-btn").removeClass("true-log");
          }
        });

        $("#register-form input").keyup(function () {
          if ($("#reg-email").val() != "" && $("#reg-password").val() != "" && $("#reg-password-confirm").val() != "" && $("#agree").is(':checked')) {
            $(".register-btn").addClass("true-log");
          } else {
            $(".register-btn").removeClass("true-log");
          }
        });

        $(".agree-label").click(function () {
          if ($("#reg-email").val() != "" && $("#reg-password").val() != "" && $("#reg-password-confirm").val() != "" && $("#agree").is(':checked')) {
            $(".register-btn").addClass("true-log");
          } else {
            $(".register-btn").removeClass("true-log");
          }
        });
        $(".login-header-btn").click(function () {
          if (($(this).attr("id") == "login-btn") && ($("#login-form").attr("data-visible") == "false")) {
            $(this).addClass("active");
            $("#register-btn").removeClass("active");
            $("#login-form").attr("data-visible", "true");
            $("#register-form").attr("data-visible", "false");
            $("#register-form").addClass("d-none");
            $("#login-form").fadeOut(0);
            $("#login-form").fadeIn();
            $("#login-form").removeClass("d-none");
          } else if (($(this).attr("id") == "register-btn") && ($("#register-form").attr("data-visible") == "false")) {
            $(this).addClass("active");
            $("#login-btn").removeClass("active");
            $("#register-form").attr("data-visible", "true");
            $("#login-form").attr("data-visible", "false");
            $("#login-form").addClass("d-none");
            $("#register-form").fadeOut(0);
            $("#register-form").fadeIn();
            $("#register-form").removeClass("d-none");
          }
        });
      });
    </script>

@endsection

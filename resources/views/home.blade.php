@extends('layouts.app')

@section('content')
    <main>


        @php $sliders=\App\Models\SliderPic::all() @endphp
        @php $slide=\App\Models\SliderPic::first() @endphp

        <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                @foreach($sliders as $slider)
                    <div class="carousel-item @if($slider->id == $slide->id) active @endif ">
                        <img src="<?= Url("{$slider->address}") ?>" class="d-block w-100" alt="Slide 1">
                    </div>
                @endforeach
            </div>
            <a class="carousel-control carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-icon fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-icon fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        @php $setting=\App\Models\Setting::first() @endphp
        <div>
            {!! $setting->name !!}
        </div>

        <div class="video-box box mt-4 mb-5">
            <div class="container">
                <div class="d-flex mb-4">
                    <span class="my-auto fas fa-video home-topic-icon"></span>
                    <h2 class="col-4e h5 my-auto text-white">ویدیو پادکست</h2>
                </div>
                <div class="row">
                    <div class="flex-container col-12 col-md-4">
                        <video style="width: 300px!important;" class="inner-element" id="video1"  controls>
                            <source src="{!!  $setting->linkVideo1 !!}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>

                    </div>

                    <div class="flex-container col-12 col-md-4">
                        <video style="width: 300px!important;" class="inner-element" id="video2"   controls>
                            <source src="{!!  $setting->linkVideo2 !!}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <div class="flex-container col-12 col-md-4">
                        <video style="width: 300px!important;" class="inner-element" id="video3"  controls>
                            <source src="{!!  $setting->linkVideo3 !!}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">

        </div>
        <div class="category-2-box box mt-4 pb-3 mb-5">
            <div class="container">
                <div class="d-flex">
                    <span class="my-auto fa fa-bars home-topic-icon"></span>
                    <h2 class="col-4e h5 my-auto text-white">دسته‌بندی</h2>
                </div>
                <div class="mt-4 category-item row mx-0 w-100">
                    <div class="col-12 col-md-3">
                        <a href="#">
                            <img class="w-100" src="<?= Url('images/content.jpg') ?>" alt="">
                        </a>
                    </div>
                    <div class="col-12 col-md-6 text-right my-auto text-white">
                        <span class="fas fa-music text-white"></span>
                        <a href="#" class="text-white h5 d-block">نام آلبوم</a>
                        <a href="#" class="text-white d-block">نام چنل</a>
                        <p class="small cat-desc mt-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe,
                            atque deleniti reprehenderit officia dolorum ea nemo unde cupiditate nulla veritatis
                            exercitationem quia doloribus repellendus labore temporibus voluptates non inventore?
                            Ea.</p>
                        <div class="d-md-flex">
                            <span class="cat-play-ic fa fa-play-circle"></span>
                            @php $categories = \App\Models\SoundCategory::first() @endphp
                            <a href="/category/{{$categories->id}}" class="my-auto h6 mr-3 upload-btn d-inline-block font-weight-normal">
                                <span>همین الان دانلود کن</span>
                                <span class="fa fa-cloud-upload-alt"></span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div class="upload-box mt-4 mb-5">
            <div class="container">
                    {!!  $setting->name2 !!}
                <div class="row col-4e text-center">
                    <div class="col-12 col-sm-4 px-5">
                        {!!  $setting->content1 !!}
                    </div>
                    <div class="col-12 col-sm-4 px-5">
                        {!!  $setting->content2 !!}
                    </div>
                    <div class="col-12 col-sm-4 px-5">
                        {!!  $setting->content3 !!}
                    </div>
                </div>
                <div class="row col-4e">
                    <a href="#" class="h6 mx-auto upload-btn d-inline-block font-weight-normal">
                        <span>همین الان دانلود کن</span>
                        <span class="fa fa-cloud-upload-alt"></span>
                    </a>
                </div>
            </div>
        </div>
    </main>

@endsection



@section('js')
<script>
  $("sample").each(function () { this.pause() });
  $("video1").each(function () { this.pause() });
  $("video2").each(function () { this.pause() });
  $("video3").each(function () { this.pause() });
</script>
    <script src="<?= Url('js/jquery.lightbox_me.js')?>"></script>

    <script type="text/javascript">
      $(function() {
        $('#btn').click(function() {
          $('#sample').lightbox_me({
            overlaySpeed: 300,
            lightboxSpeed: 'fast',
            closeClick: true,
            closeEsc: true,
            showOverlay: true,
            centered: true,
            overlayCSS: {
              'background-color': '#d35400',
              opacity: 0.6
            }
          });
        });

        $('#close').click(function() {
          $('#sample').trigger('close');
        });
      });
    </script>


@endsection

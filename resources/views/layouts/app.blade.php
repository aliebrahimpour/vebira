<!DOCTYPE html>
<html>
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= Url('css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= Url('owl/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?= Url('owl/owl.theme.default.min.css')?>">
    <link rel="stylesheet" href="<?= Url('css/style.css')?>">
    <link rel="stylesheet" href="<?= Url('css/fontawesome/all.min.css')?>">
    <link rel="stylesheet" href="<?= Url('css/fontawesome/fontawesome.min.css')?>">
    <link rel="stylesheet" href="<?= Url('css/fontawesome/v4-shims.min.css')?>">

    <script src="<?= Url('js/jquery-3.4.1.slim.min.js')?>"></script>
    <script src="<?= Url('js/popper.min.js')?>"></script>
    <script src="<?= Url('js/bootstrap.min.js')?>"></script>
    <script src="<?= Url('js/jquery-3.5.0.min.js')?>"></script>
    <script src="<?= Url('owl/owl.carousel.min.js')?>"></script>
    <link rel="stylesheet" href="<?= Url('js/fontawesome/fontawesome.min.js')?>">
    <link rel="stylesheet" href="<?= Url('js/fontawesome/v4-shims.min.js')?>">
    @yield('css')
    <style type="text/css">
        #btn, #close {
            width: 200px;
            height: 50px;
            background-color: #e1e1e1;
            cursor: pointer;
            border: none;
            display: block;
            outline: 0;
            margin: 5px auto;
        }

        #sample {
            width: 500px;
            height: 300px;
            margin: 50px auto;
            background-color: #95a5a6;
            padding: 10px;
            display: none;
        }
    </style>
</head>
<body>
<header class="myheader">
    <div class="nav-parnet">
        <nav class="main-nav w-100 position-fixed">
            <div style="max-width: 1250px" class="container">
                <div class="row text-right">
                    <div class="col-6 d-none-mobile">
                        <div class="logo-parent inline-box align-middle">
                            <a href="/">
                                <img class="nav-logo" src="<?= Url('images/logo.png')?>" alt="Site logo">
                            </a>
                        </div>
                        <div class="list-parent inline-box align-middle">
                            <ul class="list mb-0">
                                <li>
                                    @php $categories = \App\Models\SoundCategory::first() @endphp
                                    <a class="nav-link" href="/category/{{$categories->id}}">دسته بندی</a>

                                </li>
                                <li>
                                    <a href="#">برنامه سازان</a>
                                </li>
                                <li>
                                    <a href="#">وبلاگ</a>
                                </li>
                                @php $menus = \App\Models\Menu::all() @endphp
                                @php $pages = \App\Models\StaticPage::all() @endphp

                                @foreach($menus as $menu)
                                    @php $selectedMenu =  $pages->where('id',$menu->page_id)->first() @endphp

                                    <li>
                                        <a class="nav-link"
                                           href="{{ url('/').'/'.$selectedMenu->link }}">{{  $selectedMenu->name }}
                                            <span
                                                    class="sr-only">(current)</span></a>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div style="margin: auto" class="col-6 col-md-6 col-sm-6 text-left d-flex h-fit d-none-mobile">
                        <div class="search-parent mr-auto">
                            <form action="/search" method="get">
                                <input name="search" class="search-header" type="text" id="search" placeholder="جستجو">
                                <button style="padding-right: 1px" class="btn btn-default" type="submit">
                                    <span class="fa fa-search search-icon"></span>
                                </button>
                            </form>
                        </div>
                        <div class="login-parent mr-3 my-auto">
                            @if(!auth()->check())
                                <div class="login-parent my-auto">
                                    <a class="nav-link" href="/login">ورود یا ثبت نام</a>
                                </div>
                            @else
                                <div class="login-parent mr-3 my-auto">
                                    <a class="nav-link" href="/user-panel">پنل کاربری</a>
                                </div>
                            @endif
                        </div>
                        <div class="cart-parent my-auto mr-3">
                            <a href="/cart">
                                <span class="cart-show-btn fa fa-shopping-cart fa-2x"></span>
                                @php use Illuminate\Support\Facades\Session;
                                 $count = count((array)session::get('cart'))
                                @endphp
                                <span style="position: absolute;margin-top:10px;margin-right: -40px"
                                      class="badge badge-info">
                                    {{$count}}
                                </span>
                            </a>
                        </div>
                    </div>

                    <div class="col-12 d-show-mobile">
                        <div class="row nav-mobile">
                            <div class="col-3">
                                <span class="fa fa-bars bars-icon"></span>
                            </div>
                            <div class="col-4">
                                <a href="/">
                                    <img class="nav-logo" src="<?= Url('images/logo.png')?>" alt="Site logo">
                                </a>
                            </div>
                            <div class="col-5 text-left my-auto">
                                <a href="/cart">
                                    <span class="cart-show-btn fa fa-shopping-cart fa-2x"></span>
                                    <span style="position: absolute;margin-top: 10px;margin-right: -45px"
                                          class="badge badge-info">
                                        {{$count}}
                                    </span>
                                </a>
                                <span class="fa fa-search search-btn-mobile"></span>
                                @if(!auth()->check())
                                    <div class="login-parent mr-3 my-auto">
                                        <a class="nav-link" href="/login">ورود یا ثبت نام</a>
                                    </div>
                                @else
                                    <div class="login-parent mr-3 my-auto">
                                        <a class="nav-link" href="/user-panel">پنل کاربری</a>
                                    </div>
                                @endif
                            </div>


                        </div>
                        <div class="row search-input-mobile">
                            <form action="/search" method="get">
                                <input name="search" type="text" placeholder="جستجو">
                                {{--<button class="btn btn-default" type="submit">--}}
                                <span class="fa fa-times search-btn-mobile-close"></span>
                                {{--</button>--}}
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <ul class="list-mobile" data-visible="false">
        <li>
            <a href="#">دسته بندی</a>
        </li>
        <li>
            <a href="#">برنامه سازان</a>
        </li>
    </ul>
</header>
@yield('content')
<footer>
    <div class="container py-5">
        <div class="row mx-0">
            <a class="footer-logo-p" href="/">
                <img class="footer-logo" src="<?= Url('images/logo.png') ?>" alt="Site logo">
            </a>
            <a class="my-auto footer-link-top mr-1" href="/category/{{$categories->id}}">دسته‌بندی</a>
            <a class="my-auto footer-link-top mr-1" href="#">برنامه سازان</a>
        </div>
        <div class="row mx-0 mt-4">
            <div class="footer-box col-12 col-sm-4">
                <ul class="footer-list">
                    @php $menus = \App\Models\Menu::all() @endphp
                    @php $pages = \App\Models\StaticPage::all() @endphp

                    @foreach($menus as $menu)
                        @php $selectedMenu =  $pages->where('id',$menu->page_id)->first() @endphp

                        <li class="nav-item">
                            <a style="color: #fff;" class="nav-link"
                               href="{{ url('/').'/'.$selectedMenu->link }}">{{  $selectedMenu->name }} <span
                                        class="sr-only">(current)</span></a>
                        </li>
                    @endforeach
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--در رسانه‌ها--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--ارتباط باما--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                    {{--<a href="#">--}}
                    {{--وبلاگ--}}
                    {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </div>

            <div class="footer-box col-12 col-sm-4 text-right">

                <!-- <ul class="footer-list">
                    <li>
                        <a href="#">
                            پرسش‌های متداول
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            همکاری با ما
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            مسئولیت اجتماعی
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            شرایط و قوانین استفاده
                        </a>
                    </li>
                </ul> -->
                <span class="enamad-box">نماد ها</span>
                <img class="mr-2" width="100px" src="<?= Url('images/enamad.jpg') ?>" alt="">
            </div>
            <div class="footer-box col-12 col-sm-4 mr-auto text-left">
                <div class="text-center">
                    <p class="h5 text-white">جهان اثر را دنبال کنید</p>
                </div>
                <div class="footer-socials text-center mt-3">
                    <a href="#">
                        <span class="fab fa-instagram"></span>
                    </a>
                    <a href="#">
                        <span class="fab fa-telegram-plane"></span>
                    </a>
                    <a href="#">
                        <span class="fab fa-twitter"></span>
                    </a>
                    <a href="#">
                        <span class="fab fa-linkedin-in"></span>
                    </a>
                </div>
                <div class="text-center mt-3">
                    <a href="#">
                        <!-- Webira logo -->
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="copyright-box">
    <div class="container">
        <p class="mb-0">Copyright text 2019-2020</p>
    </div>
</div>
<script>
  $(document).ready(function () {
    function runOwlCarousel() {
      $(".owl-carousel-nplay, .owl-carousel-chan").owlCarousel({
        rtl: true,
        margin: 10,
        autoWidth: true,
        nav: true,
      });
      $(".owl-carousel-single").owlCarousel({
        rtl: true,
        autoWidth: true,
        items: 1
      });
      $(".owl-carousel-play").owlCarousel({
        loop: true,
        rtl: true,
        margin: 10,
        autoWidth: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true
      });
      $(".owl-carousel-play-2").owlCarousel({
        loop: true,
        rtl: true,
        margin: 10,
        merge: true,
        mergeFit: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true
      });

    }

    function changeText() {
      var text = $(".text-owl").text();
      if (text.length > 20) {
        $(".text-owl").text(function (i, origText) {
          return origText.substring(0, 20) + '...';
        });
      }
      var text = $(".cat-desc").text();
      if (text.length > 200) {
        $(".cat-desc").text(function (i, origText) {
          return origText.substring(0, 200) + '...';
        });
      }
    }

    runOwlCarousel();
    changeText();
    $(".bars-icon").click(function () {
      var list = $(".list-mobile").attr("data-visible");
      if (list == "true") {
        $(".list-mobile").attr("data-visible", "false");
        $(".list-mobile").slideUp("slow");
        $(".bars-icon").attr("class", "fa fa-bars bars-icon");
      } else {
        $(".list-mobile").attr("data-visible", "true");
        $(".list-mobile").slideDown("slow");
        $(".bars-icon").attr("class", "fa fa-times bars-icon");
      }
    });
    $(".search-btn-mobile-close").click(function () {
      $(".search-input-mobile").fadeOut();
      $(".nav-mobile").fadeIn();
    });
    $(".search-btn-mobile").click(function () {
      $(".search-input-mobile").fadeIn();
      $(".nav-mobile").fadeOut();
    });
    $(".cart-show-btn, .cart-box").hover(function () {
      $(".cart-box").show();
    }, function () {
      $(".cart-box").hide();
    });
  });
</script>
@yield('js')

</body>
</html>

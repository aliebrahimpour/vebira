<div class="sidebar col-md-2 text-right">
    <ul class="profile-nav nav flex-column py-4">
        <li class="nav-item">
            <img class="profile-img" src="<?= Url('images/Avatar.png') ?>" alt="">
        </li>
        <li class="nav-item mt-4">
            <p class="h6 col-4e">{{Auth::user()->name }}</p>
        </li>
        <li class="nav-item">
            <a class="user-type-link" href="#">کاربر عادی</a>
        </li>
        <hr class="hr-profile">
        <li class="nav-item">
            <a class="nav-link @if (strpos(request()->getRequestURI(),'/user-panel') !== false ) active @endif" href="/user-panel">داشبورد</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if (strpos(request()->getRequestURI(),'/user-panel/profile') !== false ) active @endif" href="/user-panel/profile">پروفایل</a>
        </li>
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" href="chanels.html">کانال‌ها</a>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link @if (strpos(request()->getRequestURI(),'/user-panel/upload') !== false ) active @endif" href="/user-panel/upload">آپلود</a>
        </li>
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" href="#">شنوتو پلاس</a>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
        {{--<a class="nav-link" href="#">درخواست برنامه ساز</a>--}}
        {{--</li>--}}
        <li class="nav-item @if (strpos(request()->getRequestURI(),'/user-panel/buy') !== false ) active @endif">
            <a class="nav-link" href="/user-panel/buy">گزارش خرید</a>
        </li>
        <li class="nav-item @if (strpos(request()->getRequestURI(),'/cart') !== false ) active @endif">
            <a class="nav-link" href="/cart">سبد خرید من</a>
        </li>

        <li class="nav-item">
            <form  action="{{route('logout')}}" method="POST">
                @csrf
                {{--<i class="fa fa-outdent nav-icon"></i>--}}
                <button style="border-color: #0C9A9A" class="nav-link  bg-transparent" type="submit">خروج</button>
            </form>
        </li>
    </ul>
</div>

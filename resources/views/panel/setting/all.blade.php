@extends('panel.layouts.base')

@section('script')

    <script src="<?= Url('plugins/ckeditor/ckeditor.js') ?>"></script>

    <script>
      var configShared = {
          filebrowserUploadUrl: '/panel/upload-image',
          filebrowserImageUploadUrl: '/panel/upload-image'
        },
        config1 = CKEDITOR.tools.prototypedCopy(configShared),
        config2 = CKEDITOR.tools.prototypedCopy(configShared);


      // $(".editor").each(function () {
      //   let id = $(this).attr('id');
      //   CKEDITOR.replace(id, config1);
      // });

      CKEDITOR.replace('name', config1);
      CKEDITOR.replace('name2', config2);
      CKEDITOR.add
    </script>

    <script>
      CKEDITOR.replace('content1', {
        filebrowserUploadUrl: '/panel/upload-image',
        filebrowserImageUploadUrl: '/panel/upload-image'
      });
    </script>
    <script>
      CKEDITOR.replace('content2', {
        filebrowserUploadUrl: '/panel/upload-image',
        filebrowserImageUploadUrl: '/panel/upload-image'
      });
    </script>
    <script>
      CKEDITOR.replace('content3', {
        filebrowserUploadUrl: '/panel/upload-image',
        filebrowserImageUploadUrl: '/panel/upload-image'
      });
    </script>
@endsection


@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>تنظیمات عمومی سایت</h2>
        </div>
        <form class="form-horizontal" action="{{ route('public.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('layouts.errors')
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="name" class="control-label">عنوان اصلی</label>
                    <textarea  class="editor form-control" name="name" id="name" placeholder="عنوان را وارد کنید">{!!   $setting->name !!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="linkVideo1" class="control-label">ویدئو سمت راست</label>
                    <input type="text" class="form-control" name="linkVideo1" id="linkVideo1" placeholder="لینک را وارد کنید" value="{{ $setting->linkVideo1 }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="linkVideo2" class="control-label">ویدئو وسط صفحه</label>
                    <input type="text" class="form-control" name="linkVideo2" id="linkVideo2" placeholder="لینک را وارد کنید" value="{{ $setting->linkVideo2 }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="linkVideo3" class="control-label">ویدئو سمت چپ</label>
                    <input type="text" class="form-control" name="linkVideo3" id="linkVideo3" placeholder="لینک را وارد کنید" value="{{ $setting->linkVideo3 }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="name2" class="control-label">عنوان دوم</label>
                    <textarea type="text" class="editor form-control" name="name2" id="name2" placeholder="عنوان را وارد کنید">{!! $setting->name2 !!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="content1" class="control-label">محتوا سمت راست</label>
                    <textarea type="text" class="form-control" name="content1" id="content1" placeholder="لینک را وارد کنید" >{!! $setting->content1 !!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="content2" class="control-label">محتوا وسط صفحه</label>
                    <textarea type="text" class="form-control" name="content2" id="content2" placeholder="لینک را وارد کنید">{!! $setting->content2 !!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="content3" class="control-label">محتوا سمت چپ</label>
                    <textarea type="text" class="form-control" name="content3" id="content3" placeholder="لینک را وارد کنید" >{!! $setting->content3 !!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="label" class="control-label">توضیحات کوتاه</label>
                    <textarea rows="5" class="form-control" name="label" id="label" placeholder="توضیحات را وارد کنید">{{ old('label') }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger">ارسال</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@extends('panel.layouts.base')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>اسلایدر ها</h2>
            <div class="btn-group">
                <a href="{{ route('slider.create') }}" class="btn btn-sm btn-danger">ایجاد اسلایدر</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>نام اسلایدر</th>
                    <th>موقعیت مکانی</th>
                    <th>توضیحات</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sliders as $slider)
                    <tr>
                        <td>{{ $slider->name }}</td>
                        <td>{{ $slider->location }}</td>
                        <td>{{ $slider->label }}</td>
                        <td>
                            <form action="{{ route('slider.destroy'  , $slider->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    {{--<a href="{{ route('role.edit' ,  $slider->id) }}"  class="btn btn-primary">ویرایش</a>--}}
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="text-align: center">
            {!! $sliders->render() !!}
        </div>
    </div>
@endsection

@extends('panel.layouts.base')

@section('css')
    {{--<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">--}}
    <link href="<?= Url('css/dropzone.css')?>" rel="stylesheet">
    <!-- Bootstrap time Picker -->
    {{--<link rel="stylesheet" href="<?= Url('plugins/timepicker/bootstrap-timepicker.min.css') ?>">--}}
    <!-- Persian Data Picker -->
    {{--<link rel="stylesheet" href="<?= Url('dist/css/persian-datepicker.min.css') ?>">--}}
    <!-- Select2 -->
    {{--<link rel="stylesheet" href="<?= Url('plugins/select2/select2.min.css') ?>">--}}
@endsection

@section('script')
    <script src="<?= Url('js/dropzone.js')?>"></script>

    <!-- ckeditor -->
    {{--<script src="<?= Url('plugins/ckeditor/ckeditor.js') ?>"></script>--}}
    {{--<script>--}}
    {{--CKEDITOR.replace('address', {--}}
    {{--filebrowserUploadUrl: '/panel/upload-image',--}}
    {{--filebrowserImageUploadUrl: '/panel/upload-image'--}}
    {{--});--}}
    {{--</script>--}}

    <!-- Select2 -->
    {{--<script src="<?= Url('plugins/select2/select2.full.min.js') ?>"></script>--}}
    {{--<script src="<?= Url('plugins/daterangepicker/daterangepicker.js') ?>"></script>--}}
    {{--<script src="<?= Url('dist/js/persian-date.min.js') ?>"></script>--}}
    {{--<script src="<?= Url('dist/js/persian-datepicker.min.js') ?>"></script>--}}
    {{--<script type="text/javascript">--}}
    {{--$(document).ready(function () {--}}
    {{--$(".birthday").pDatepicker({--}}
    {{--format: 'YYYY/MM/DD',--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div style="margin-bottom: 20px" class="page-header text-center head-section">
            <h2>ثبت اسلایدر جدید</h2>
        </div>
        <form class="form-horizontal" action="{{route('slider.store')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts.errors')
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="sliderName" class="control-label">نام اسلایدر</label>
                        <input type="text" class="form-control" name="sliderName" id="sliderName"
                               placeholder="نام اسلایدر را وارد کنید" value="{{ old('sliderName') }}">
                    </div>
                </div>

                {{--<div class="col-sm-4">--}}
                {{--<div class="form-group">--}}
                {{--<label for="location" class="control-label">موقعیت مکانی</label>--}}
                {{--<input type="text" class="form-control" name="location" id="location"--}}
                {{--placeholder="ایمیل را وارد کنید"--}}
                {{--value="{{ old('location') }}">--}}
                {{--</div>--}}
                {{--</div>--}}

                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="location" class="control-label">موقعیت مکانی</label>
                        <select name="location" class="form-control select2" style="width: 100%;">
                            <option value="index" selected="selected">صفحه اصلی</option>
                        </select>
                    </div>
                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <label for="description" class="control-label">توضیحات</label>
                    <textarea rows="2" class="form-control" name="description" id="description"
                              placeholder="توضیحات را وارد کنید">{{ old('description') }}</textarea>
                </div>
                <br>

            </div>
            <div class="form-group">
                    <div class="fallback">
                        <input name="files[]" type="file" multiple />
                    </div>
            </div>



            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger">ارسال</button>
                </div>
            </div>
        </form>
    </div>
@endsection



@extends('panel.layouts.base')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>دسته بندی ها</h2>
            <div class="btn-group">
                <a href="{{ route('panelCategory.create') }}" class="btn btn-sm btn-success">ایجاد دسته بندی</a>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>نام دسته بندی</th>
                    <th>توضیحات</th>
                    <th>تنظیمات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($SoundCategories as $SoundCategory)
                    <tr>
                        <td>{{ $SoundCategory->name }}</td>
                        <td>{{ $SoundCategory->description }}</td>
                        <td>
                            <form action="{{ route('panelCategory.destroy'  , $SoundCategory->id) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('panelCategory.edit' ,  $SoundCategory->id) }}"  class="btn btn-primary">ویرایش</a>
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div style="text-align: center">
            {!! $SoundCategories->render() !!}
        </div>
    </div>
@endsection

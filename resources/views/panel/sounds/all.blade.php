@extends('panel.layouts.base')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div style="margin-bottom: 20px" class="page-header text-center head-section">
            <h2>لیست پادکست ها </h2>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>نام پادکست</th>
                    <th>وضعیت</th>
                    <th>نوع</th>
                    <th>دسته بندی</th>
                    <th>اطلاعات بیشتر</th>
                </tr>
                </thead>
                <tbody>
                @foreach($sounds as $sound)
                    <tr>
                        <td>{{ $sound->title }}</td>
                        <td>
                            @if($sound->enabled)
                                <span class="badge badge-success">فعال</span>
                            @else
                                <span class="badge badge-danger">غیر فعال</span>
                            @endif

                        </td>

                        <td>
                            @if(!$sound->type)
                                <span class="badge badge-warning">رایگان</span>
                            @else
                                <span class="badge badge-danger">نقدی</span>
                            @endif

                        </td>
                        @php $category = \App\Models\SoundCategory::whereId($sound->category_id)->first()@endphp
                        <td>{{$category->name }}</td>


                        <td>
                            <div class="input-group-prepend">
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                    امکانات
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @can('sound-accept')
                                        <li class="dropdown-item">
                                            @if($sound->enabled == 0)
                                                {{ Form::open([ 'method'  => 'post', 'route' => [ 'podcast.accept', $sound->id ] ]) }}
                                                {{ csrf_field() }}
                                                <div class="btn-group btn-group-xs">
                                                    <button type="submit" class="btn btn-success">فعال سازی</button>
                                                </div>
                                                {{ Form::close() }}
                                            @elseif($sound->enabled == 1)
                                                {{ Form::open([ 'method'  => 'post', 'route' => [ 'podcast.unAccept', $sound->id ] ]) }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger">غیر فعال سازی</button>
                                                {{ Form::close() }}
                                            @endif
                                        </li>
                                    @endcan
                                    <li class="dropdown-item">
                                        {{ Form::open([ 'method'  => 'delete', 'route' => [ 'podcast.destroy', $sound->id ] ]) }}
                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}
                                        <div class="btn-group btn-group-xs">
                                            @can('sound-edit')
                                                <a href="{{ route('podcast.edit', $sound->id) }}"
                                                   class="btn btn-primary">ویرایش</a>
                                            @endcan
                                            @can('sound-delete')
                                                <button type="submit" class="btn btn-danger">حذف</button>
                                            @endcan
                                        </div>
                                        {{ Form::close() }}
                                    </li>
                                </ul>
                            </div>
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div style="text-align: center">
            {!! $sounds->render() !!}
        </div>
    </div>


@endsection

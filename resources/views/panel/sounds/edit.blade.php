@extends('panel.layouts.base')

@section('css')
    {{--<!-- Bootstrap time Picker -->--}}
    {{--<link rel="stylesheet" href="<?= Url('plugins/timepicker/bootstrap-timepicker.min.css') ?>">--}}
    {{--<!-- Persian Data Picker -->--}}
    {{--<link rel="stylesheet" href="<?= Url('dist/css/persian-datepicker.min.css') ?>">--}}
    {{--<!-- Select2 -->--}}
    {{--<link rel="stylesheet" href="<?= Url('plugins/select2/select2.min.css') ?>">--}}
@endsection

@section('script')
    <!-- ckeditor -->
    <script src="<?= Url('plugins/ckeditor/ckeditor.js') ?>"></script>
    <script>
      CKEDITOR.replace('description', {
        filebrowserUploadUrl: '/panel/upload-image',
        filebrowserImageUploadUrl: '/panel/upload-image'
      });
        {{--</script>--}}
        {{--<script>--}}
        {{--CKEDITOR.replace('description', {--}}
        {{--filebrowserUploadUrl: '/panel/upload-image',--}}
        {{--filebrowserImageUploadUrl: '/panel/upload-image'--}}
        {{--});--}}
        {{--</script>--}}
        {{--<script>--}}
        {{--CKEDITOR.replace('designerComment', {--}}
        {{--filebrowserUploadUrl: '/panel/upload-image',--}}
        {{--filebrowserImageUploadUrl: '/panel/upload-image'--}}
        {{--});--}}
    </script>
    <script>
      $(document).ready(function () {
        $("#enable").click(function () {
          $("#price").attr("disabled", false);
        });

        $("#disable").click(function () {
          $("#price").attr("disabled", true);
        });

      });
    </script>


    {{--<!-- Select2 -->--}}
    {{--<script src="<?= Url('plugins/select2/select2.full.min.js') ?>"></script>--}}
    {{--<script src="<?= Url('plugins/daterangepicker/daterangepicker.js') ?>"></script>--}}
    {{--<script src="<?= Url('dist/js/persian-date.min.js') ?>"></script>--}}
    {{--<script src="<?= Url('dist/js/persian-datepicker.min.js') ?>"></script>--}}
    {{--<script type="text/javascript">--}}
    {{--$(document).ready(function () {--}}
    {{--$(".birthday").pDatepicker({--}}
    {{--format: 'YYYY/MM/DD',--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div style="margin-bottom: 20px" class="page-header text-center head-section">
            <h2>ویرایش پادکست </h2>
        </div>
        <form class="form-horizontal" action="{{route('podcast.update' ,  $sound->id )}}" method="post"
              enctype="multipart/form-data">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            @include('layouts.errors')
            <input hidden name="sound_id" value="{{$sound->id}}">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="title" class="control-label"> عنوان پادکست</label>
                        <input type="text" class="form-control" name="title" id="title"
                               placeholder="عنوان پادکست را وارد کنید" value="{{ $sound->title }}">
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group">
                        <label for="category_id" class="control-label">دسته بندی</label>
                        <select name="category_id" class="form-control select2" style="width: 100%;">
                            @php $SoundCategorys = \App\Models\SoundCategory::all() @endphp

                            @foreach($SoundCategorys as $SoundCategory)
                                <option
                                        @if ($sound->category_id == $SoundCategory->id)
                                        selected="selected"
                                        @endif
                                        value="{{$SoundCategory->id}}">{{$SoundCategory->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="description" class="control-label">توضیحات</label>
                    <textarea rows="5" class="form-control" name="description" id="description"
                              placeholder="توضیحات را وارد کنید">{{ $sound->description }}</textarea>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="podcast" class="control-label">ثبت پادکست</label>
                        <input type="file" class="form-control-file" name="podcast" id="podcast">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="picture" class="control-label">تصویر منتخب</label>
                        <input type="file" class="form-control-file" name="picture" id="picture">
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="speakers" class="control-label">گویندگان:</label>
                        <input type="text" class="form-control" name="speakers" id="speakers"
                               placeholder="اسامی گویندگان را وارد کنید" value="{{ $sound->speakers }}">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="type">نوع دوره:</label>
                        <br>
                        <Input type='Radio' Name='type' value='0' id="disable">رایگان
                        <input type='Radio' Name='type' value='1' id="enable"> نقدی
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="price" class="control-label">قیمت (تومان):</label>
                        <input type="number" class="form-control" name="price" id="price"
                               placeholder="قیمت را وارد کنید" value="{{ $sound->price }}">
                    </div>
                </div>

            </div>


            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger">ارسال</button>
                </div>
            </div>
        </form>
    </div>
@endsection



@extends('layouts.app')

@section('content')
    <main>
        @include('layouts.header')

        <div class="container mt-4">
            <p class="line line-red col-4e">سبد خرید</p>
            <div class="row mx-0 mt-4">
                <div class="col-12 col-md-9 px-0">


                    <?php $total_price = 0; ?>
                    @if(is_array(session()->get('cart')) || is_object(session()->get('cart')))
                        @foreach(session()->get('cart') as $key=>$value)
                            <?php $product = \App\Models\Sound::whereId($key)->first(); ?>
                            <div class="w-100 row mx-0 cart-item">
                                <div class="col-12 col-md-2 my-auto">
                                    <img class="w-100" src="<?= Url("{$product->picture}")?>" alt="">
                                </div>
                                <div class="col-12 col-md-5 text-right my-auto">
                                    <p class="mb-2 col-4e">{{$product->title}}</p>
                                    @php $SoundCategory = \App\Models\SoundCategory::whereId($product->category_id)->first() @endphp

                                    <p class="mb-2 col-4e small">{{$SoundCategory->name}}</p>
                                </div>
                                <div class="col-5 col-md-5 my-auto">
                                    <p>{{$product->price}} تومان</p>
                                    <?php $total_price += $product->price; ?>

                                </div>
                                <div class="col-6 col-md-12 my-auto">
                                    <a href="podcast/{{$product->id}}/removetocard">
                                        <p>حذف از سبد خرید</p>
                                    </a>
                                </div>

                            </div>

                        @endforeach


                </div>
                <div class="col-12 col-md-3">
                    <div class="cart-factor col-4e small">
                        <div class="d-flex">
                            <span>مجموع کل</span>
                            <span class="col-f34 mr-auto">{{$total_price}} تومان</span>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>


            <form class="row mx-0 mt-4 mb-4" action="/product/payment" method="post">
                @csrf
                <input name="total_price" type="hidden" value="{{$total_price}}">
                <button class="b-r-50 green-btn mr-auto" type="submit">خرید خود را نهایی کنید</button>
            </form>
        </div>
        @endif

    </main>

@endsection

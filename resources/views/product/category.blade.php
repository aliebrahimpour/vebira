@extends('layouts.app')

@section('content')
    <main>
        @include('layouts.header')

        <div class="items-box box mt-5 mb-5">
            <div class="container">
                <div class="row mb-4 items-box-header mx-0 py-3">
                    <div class="col-12 col-md-7">
                        <p class="line mb-0">دسته بندی ها</p>
                        <table>
                            <tr>
                                @php $count = 1; @endphp
                                @php $categories = \App\Models\SoundCategory::all() @endphp

                                @foreach($categories as $category)
                                    @if ($count%3 != 0)

                                        <td >
                                            <a @if($SoundCategories->id == $category->id) class="active" @endif href="{{$category->id}}">{{$category->name}}</a>
                                        </td>
                                    @else
                                        <td>
                                            <a @if($SoundCategories->id == $category->id) class="active" @endif href="{{$category->id}}">{{$category->name}}</a>
                                        </td>
                            </tr>
                            <tr>

                            @endif
                            @php $count++; @endphp
                            @endforeach
                        </table>
                    </div>
                    <div class="col-12 col-md-5">
                        <p class="line mb-0">زیربخش ها</p>
                        <table>
                            @php $sounds = \App\Models\Sound::whereCategory_id($SoundCategories->id)->get() @endphp

                            @php $count = 1; @endphp

                            <tr>
                                @foreach($sounds as $sound)
                                    @if ($count%2 != 0)
                                        <td>
                                            <a  href="/podcast/{{$sound->id}}">{{$sound->title}}</a>
                                        </td>
                                    @else
                                        <td>
                                            <a  href="/podcast/{{$sound->id}}">{{$sound->title}}</a>
                                        </td>
                            </tr>
                            <tr>

                            @endif
                            @php $count++; @endphp
                            @endforeach

                        </table>
                    </div>
                </div>
                <div style="text-align: right" class="card mb-4 shadow-sm">
                    <div class="card-header">
                        <h4 class="my-0 font-weight-normal">عنوان</h4>
                    </div>
                    <div class="card-body">
                        <h1 class="card-title pricing-card-title"> <small class="text-muted">توضیحات</small></h1>
                        <ul class="list-unstyled mt-3 mb-4">

                            <a href="#">
                            <img style="height: 100px;width: 200px;"src="<?= Url('images/google.png') ?>" alt="">
                            </a>

                            <a href="#">
                            <img style="height: 100px;width: 200px;" src="<?= Url('images/dl_link.png') ?>" alt="">
                            </a>
                        </ul>
                        {{--<button type="button" class="btn btn-lg btn-block btn-outline-primary">Sign up for free</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection



@section('js')
@endsection

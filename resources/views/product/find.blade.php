@extends('layouts.app')

@section('content')
    <main>
        <div class="category-box box mt-5 mb-5">
            <div class="container">
                <div class="d-flex mb-4">
                    <h2 class="col-4e h5 my-auto">نتایج جستجو</h2>
                </div>
                @foreach($sounds as $sound)
                    @php $SoundCategory = \App\Models\SoundCategory::whereId($sound->category_id)->first() @endphp
                    <p class="line">{{$SoundCategory->name}}</p>
                    <div class="row py-3">
                        <div class="col-md-2 p-0 col-12 podcast-profile-item text-center">
                            <a href="/podcast/{{$sound->id}}">
                                <span class="fa fa-search podcast-profile-icon"></span>
                                <p class="podcast-profile-title">{{$sound->title}}</p>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>


@endsection

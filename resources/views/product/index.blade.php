@extends('layouts.app')

@section('content')

    <main>
        @include('layouts.header')
        <div class="podcast-head box py-4 mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <img style="height: 400px" src="<?= Url("{$Product->picture}") ?>" class="d-block w-100" alt="Slide 1">
                    </div>
                    <div class="col-12 col-md-4 text-right">
                        <p class="h6">
                            {{$Product->title}}
                        </p>
                        <p class="small">
                            {{--<span>برند : </span>--}}
                            {{--<a href="#">ببم</a>--}}
                            <span class="mr-4">دسته بندی : </span>
                            @php $SoundCategory = \App\Models\SoundCategory::whereId($Product->category_id)->first() @endphp
                            <a href="/category/{{$SoundCategory->id}}">
                                {{$SoundCategory->name}}
                            </a>
                        </p>



                    </div>
                    <div class="col-12 p-0 col-md-4 podcast-item">
                        <div class="p-3 pod-item">
                            <p class="mb-2 h6">
                                <span>گوینده:</span>
                                <span class="font-weight-bold">{{$Product->speakers}}</span>
                            </p>
                        </div>
                        <div class="p-3 pod-item">
                            @if($Product->price != 0)

                            <p>
                                <span class="col-4e">قیمت مصرف کننده</span>
                            </p>
                            <p class="mb-0 h4 text-left col-f34">
                                <span>{{$Product->price}}</span>
                                <span>تومان</span>
                            </p>

                            @else
                                <p class="mb-0 h4 text-left col-f34">
                                    {{--<span>رایگان</span>--}}
                                    <a download="{{$Product->podcast}}"  href="{!!$Product->podcast!!}">
                                <span>دانلود</span></a>
                                </p>

                            @endif
                        </div>
                        <div class="p-3">
                            @if($Product->price != 0)
                                @if($exist != 1)
                            <a href="/podcast/{{$Product->id}}/addtocard" class="add-store">
                                افزودن به سبد خرید
                            </a>
                                    @else
                                    <a href="/cart" class="add-store">
                                        ادامه فرایند خرید
                                    </a>
                                    @endif
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="review-box box">
            <div class="container">
                <ul class="nav nav-tabs pr-0" id="reviewTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" role="tab"
                           aria-controls="review" aria-selected="true">توضیحات</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="comment-tab" data-toggle="tab" href="#comment" role="tab"
                           aria-controls="comment" aria-selected="false">نظرات کاربران</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-right py-4" id="review" role="tabpanel"
                         aria-labelledby="review-tab">
                        {!!  $Product->description !!}

                    </div>
                    <div class="tab-pane fade text-right p-4" id="comment" role="tabpanel"
                         aria-labelledby="comment-tab">
                        <div class="row">
                            <div class="col-12 col-md-6 col-4e">
                                <p>شما هم می‌توانید در مورد این پادکست نظر بدهید.</p>
                                <form class="form-horizontal" action="{{route('comment.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @include('layouts.errors')

                                    <label for="name" class="control-label">نام</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="نام خود را وارد کنید" value="{{ old('name')}}">
                                    <label for="email" class="control-label">ایمیل</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="ایمیل را وارد کنید"
                                           value="{{ old('email')}}">
                                    <label for="body" class="control-label">نظر</label>
                                    <textarea rows="2" class="form-control" name="body" id="body"
                                              placeholder="نظر خود را وارد کنید">{{ old('body') }}</textarea>
                                    <div class="form-group">
                                        <br>
                                        <button type="submit" class="btn btn-success">ارسال</button>
                                    </div>
                                    <input type="hidden" name="commentable_id" value="{{ $Product->id }}">
                                    <input type="hidden" name="commentable_type" value="{{ get_class($Product) }}">

                                </form>
                            </div>
                        </div>
                        <div class="comments mt-5 col-4e">
                            <p class="h5 ">نظرات</p>
                            <hr>

                            @foreach($comments as $comment)
                                @if($comment->parent_id == 0)
                                    <div class="cm-item d-flex">
                                        <div class="col-4">
                                            <p class="font-weight-bold mb-1">{{$comment->name}}</p>
                                            <p class="small">{{jdate($comment->created_at)->ago()}}</p>
                                        </div>
                                        <div class="col-8">
                                            {{--<p class="h6">عنوان</p>--}}
                                            {{--<hr>--}}
                                            <p>{{$comment->body}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection



@section('js')
    <script>
      $(document).ready(function () {
        function runOwlCarousel() {
          $(".owl-carousel-nplay, .owl-carousel-chan").owlCarousel({
            rtl: true,
            margin: 10,
            autoWidth: true,
            nav: true,
          });
          $(".owl-carousel-single").owlCarousel({
            rtl: true,
            autoWidth: true,
            items: 1
          });
          $(".owl-carousel-play").owlCarousel({
            loop: true,
            rtl: true,
            margin: 10,
            autoWidth: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true
          });

        }

        function changeText() {
          var text = $(".text-owl").text();
          if (text.length > 20) {
            $(".text-owl").text(function (i, origText) {
              return origText.substring(0, 20) + '...';
            });
          }
        }

        runOwlCarousel();
        changeText();
        $(".bars-icon").click(function () {
          var list = $(".list-mobile").attr("data-visible");
          if (list == "true") {
            $(".list-mobile").attr("data-visible", "false");
            $(".list-mobile").slideUp("slow");
            $(".bars-icon").attr("class", "fa fa-bars bars-icon");
          } else {
            $(".list-mobile").attr("data-visible", "true");
            $(".list-mobile").slideDown("slow");
            $(".bars-icon").attr("class", "fa fa-times bars-icon");
          }
        });
      });
    </script>
@endsection

@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="sidebar-right"></div>
                @include('layouts.userSidebar')

                <div class="col-md-10 dashboard p-4 text-right">
                    <p class="line col-4e font-weight-bold">گزارش خرید</p>
                    {{--<div class="row w-100 mx-0">--}}
                    {{--<div class="col-12 col-md-3 text-center">--}}
                    {{--<span class="report-btn">0 دنبال کنندگان</span>--}}
                    {{--</div>--}}
                    {{--<div class="col-12 col-md-3 text-center">--}}
                    {{--<span class="report-btn">0 میانگین پخش هر اپیزود</span>--}}
                    {{--</div>--}}
                    {{--<div class="col-12 col-md-3 text-center">--}}
                    {{--<span class="report-btn">0 پخش پادکست در شنوتو</span>--}}
                    {{--</div>--}}
                    {{--<div class="col-12 col-md-3 text-center">--}}
                    {{--<span class="report-btn">0 مجموع دفعات پخش</span>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row mx-0 w-100 mt-4">--}}
                    <div class="col-12 col-md-5">
                        <div class="album-report">
                            <div class="album-report-title">
                                <p class="mb-0">همه آلبوم ها:</p>
                            </div>


                            @foreach($payments  as $payment)

                                @php $products =  explode(",",$payment->code); @endphp
                                @foreach($products  as $p)
                                    @php $product = \App\Models\Sound::whereId($p)->first()  @endphp
                                    <a href="{{$product->id}}">
                                        <div class="d-flex album-report-item">
                                            <img width="20" src="<?= Url("{$product->picture}") ?>" alt="">
                                            <p class="my-auto mr-2">{{$product->title}}</p>
                                            <a class="my-auto mr-2" href="{{$product->podcast}}">
                                                <p>دانلود</p></a>
                                        </div>
                                    </a>
                                @endforeach
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </main>

@endsection

@section('js')
@endsection

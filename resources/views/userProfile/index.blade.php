@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="sidebar-right"></div>
                @include('layouts.userSidebar')
                <div class="col-md-10 dashboard p-4">
                    <img class="top-img" src="<?= Url('images/dashboard.png') ?>" alt="">
                    {{--<div class="row mt-4">--}}
                    {{--<div class="dashboard-top-item col-md-4 col-12 text-center">--}}
                    {{--<a id="album-btn" class="active" href="#album">آلبوم ها</a>--}}
                    {{--</div>--}}
                    {{--<div class="dashboard-top-item col-md-4 col-12 text-center">--}}
                    {{--<a id="liked-btn" href="#liked">پسندیده شده‌ها</a>--}}
                    {{--</div>--}}
                    {{--<div class="dashboard-top-item col-md-4 col-12 text-center">--}}
                    {{--<a id="chanel-btn" href="#chanel">کانال‌های دنبال شده</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="row mx-0 mt-5">
                        <p class="item-topic line w-100 col-4e">آلبوم ها</p>
                    </div>
                    @php use Illuminate\Support\Facades\Auth;$sounds=\App\Models\Sound::where('user_id','=',Auth::user()->id)->get() @endphp
                    @foreach($sounds as $sound)
                        <div id="albums" class="row mx-0" data-visible="false">
                            <div class="row py-3 album-item w-100 mx-0 mt-4">
                                <div class="col-md-2">
                                    <a href="#">
                                        <img class="album-img" src="<?= Url('images/dashboard.png') ?>" alt="">
                                    </a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a class="album-name" href="#">{{$sound->title}}</a>
                                    <p class="mt-2 album-like">
                                        <span class="fa fa-soundcloud"></span>
                                        <span class="ml-3">{{$sound->speakers}}</span>
                                        {{--<span class="far fa-play-circle"></span>--}}
                                        {{--<span class="">0</span>--}}
                                    </p>
                                    <p class="album-desc col-4e">
                                        {!! $sound->description !!}
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <form action="{{route('delete.podcast')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <input type="hidden" name="_method" value="delete"/>
                                        <input type="hidden" name="sound_id" value="{{$sound->id}}"/>
                                        <button type="submit" class="delete-album-btn mt-2">حذف آلبوم</button>

                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--<div id="liked-album" class="row mx-0" data-visible="false">--}}
                    {{--<div class="row py-3 album-item w-100 mx-0 mt-4">--}}
                    {{--<div class="col-md-2">--}}
                    {{--<a href="#">--}}
                    {{--<img class="album-img" src= "<?= Url('images/dashboard.png') ?>" alt="">--}}
                    {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-6 text-right">--}}
                    {{--<a class="album-name" href="#">اسم آلبوم</a>--}}
                    {{--<p class="mt-2 album-like">--}}
                    {{--<span class="fa fa-heart"></span>--}}
                    {{--<span class="ml-3">0</span>--}}
                    {{--<span class="far fa-play-circle"></span>--}}
                    {{--<span class="">0</span>--}}
                    {{--</p>--}}
                    {{--<p class="album-desc col-4e">--}}
                    {{--Lorem ipsum dolor sit amet consectetur, adipisicing elit. Reprehenderit unde perspiciatis et sed ad recusandae omnis repellendus error, soluta voluptas non fuga aperiam, dignissimos cumque minus quis alias modi assumenda!--}}
                    {{--</p>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<div id="chanels" class="row mx-0 py-3" data-visible="false">--}}
                    {{--<div class="row w-100 mx-0">--}}
                    {{--<div class="chanel-item col-12 col-md-2 text-center">--}}
                    {{--<a href="#">--}}
                    {{--<img class="chanel-img" src="<?= Url('images/dashboard.png') ?>" alt="">--}}
                    {{--<p class="chanel-name pt-2">اسم چنل</p>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="chanel-item col-12 col-md-2 text-center">--}}
                    {{--<a href="#">--}}
                    {{--<img class="chanel-img" src="<?= Url('images/dashboard.png') ?>" alt="">--}}
                    {{--<p class="chanel-name pt-2">اسم چنل</p>--}}
                    {{--</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </main>

@endsection



@section('js')
    {{--<script>--}}
    {{--$(document).ready(function(){--}}
    {{--$("#liked-album, #chanels").fadeOut(0);--}}
    {{--var text = $(".album-desc").text();--}}
    {{--if(text.length > 100){--}}
    {{--$(".album-desc").text(function(i, origText){--}}
    {{--return origText.substring(0,100) + '...';--}}
    {{--});--}}
    {{--}--}}
    {{--$(".dashboard-top-item a").click(function(){--}}
    {{--if(($(this).attr("id") == "album-btn") && ($("#albums").attr("data-visible") == "false")){--}}
    {{--$(this).addClass("active");--}}
    {{--$("#liked-btn, #chanel-btn").removeClass("active");--}}
    {{--$("#albums").attr("data-visible", "true");--}}
    {{--$("#liked-album, #chanels").attr("data-visible", "false");--}}
    {{--$("#liked-album, #chanels").addClass("d-none");--}}
    {{--$("#albums").fadeOut(0);--}}
    {{--$("#albums").fadeIn();--}}
    {{--$("#albums").removeClass("d-none");--}}
    {{--$(".item-topic").text("آلبوم ها");--}}
    {{--}--}}

    {{--else if(($(this).attr("id") == "liked-btn") && ($("#liked-album").attr("data-visible") == "false")){--}}
    {{--$(this).addClass("active");--}}
    {{--$("#album-btn, #chanel-btn").removeClass("active");--}}
    {{--$("#liked-album").attr("data-visible", "true");--}}
    {{--$("#albums, #chanels").attr("data-visible", "false");--}}
    {{--$("#albums, #chanels").addClass("d-none");--}}
    {{--$("#liked-album").fadeOut(0);--}}
    {{--$("#liked-album").fadeIn();--}}
    {{--$("#liked-album").removeClass("d-none");--}}
    {{--$(".item-topic").text("پسندیده شده‌ها");--}}
    {{--}--}}

    {{--else if(($(this).attr("id") == "chanel-btn") && ($("#chanels").attr("data-visible") == "false")){--}}
    {{--$(this).addClass("active");--}}
    {{--$("#album-btn, #liked-btn").removeClass("active");--}}
    {{--$("#chanels").attr("data-visible", "true");--}}
    {{--$("#albums, #liked-album").attr("data-visible", "false");--}}
    {{--$("#albums, #liked-album").addClass("d-none");--}}
    {{--$("#chanels").fadeOut(0);--}}
    {{--$("#chanels").fadeIn();--}}
    {{--$("#chanels").removeClass("d-none");--}}
    {{--$(".item-topic").text("دنبال شده‌ها");--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}

@endsection

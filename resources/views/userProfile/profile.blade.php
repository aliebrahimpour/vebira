@extends('layouts.app')

@section('content')

    <main>
    <div class="container">
        <div class="row">
            <div class="sidebar-right"></div>
            @include('layouts.userSidebar')

            <div class="col-md-10 dashboard p-4">
                <p class="line col-4e">اطلاعات شخصی</p>
                    <form class="text-right w-100 edit-profile-form col-4e"
                          action="{{route('profile.update', auth()->user()->id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        @include('layouts.errors')
                        <input hidden value="1"  name="enabled">
                        <input hidden value="{{auth()->user()->id}}"  name="user_id">

                        <div class="row">
                        <div class="col-12 col-md-2">
                            <label for="img" class="change-profile-img">
                                <input class="d-none" type="file" name="img" id="img">
                                <img class="" src="<?= Url('images/Avatar.png') ?>" alt="">
                                {{--<span class="small col-4e">تغییر عکس</span>--}}
                            </label>
                        </div>
                        <div class="col-12 col-md-5">
                            <div class="form-group">
                                <label for="username">ایمیل کاربری</label>
                                <input type="email"  class="form-control text-left" name="email" id="username" value="{{auth()->user()->email}}">
                            </div>
                            <div class="form-group">
                                <label for="phone">شماره موبایل</label>
                                <input dir="ltr" type="text" class="form-control text-left" name="mobile" id="mobile" value="{{auth()->user()->mobile}}" placeholder="09*********">
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="age">بازه سنی</label>--}}
                                {{--<select class="form-control" id="age">--}}
                                    {{--<option value="">انتخاب کنید</option>--}}
                                    {{--<option value="">10 تا 20 سال</option>--}}
                                    {{--<option value="">20 تا 30 سال</option>--}}
                                    {{--<option value="">30 تا 40 سال</option>--}}
                                    {{--<option value="">بالای 40 سال</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-12 col-md-5">
                            <div class="form-group">
                                <label for="name">نام و نام خانوادگی</label>
                                <input type="text" value="{{auth()->user()->name}}" name="userName" class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="gender">جنسیت</label>
                                <select name="sex" class="form-control" id="gender">
                                    <option value="1">مرد</option>
                                    <option value="0">زن</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-10 mr-md-auto">
                            <div class="form-group">
                                <label for="about-me">درباره شما</label>
                                <textarea name="address" class="form-control" id="about-me" rows="3">{{auth()->user()->address}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0">
                        <input class="mr-auto green-btn" type="submit" value="ثبت">
                    </div>
                </form>
                {{--<p class="line col-4e mt-5 mb-2">علاقه‌مندی‌ها</p>--}}
                {{--<form class="text-right w-100 edit-profile-form col-4e">--}}
                    {{--<p class="col-4e">کاربر گرامی برای ارائه خدمات بهتر، موضوعات مورد علاقه خود را انتخاب نمایید:</p>--}}
                    {{--<div class="row mx-0">--}}
                        {{--<p class="col-f34">پادکست</p>--}}
                    {{--</div>--}}
                    {{--<div class="row mx-0">--}}
                        {{--<div class="row w-100 mr-md-5">--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-2 p-0 col-12 podcast-profile-item text-center" data-checked="false" data-category="managment">--}}
                                {{--<span class="fa fa-search"></span>--}}
                                {{--<p>مدیریت و کارآفرینی</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row mx-0 mt-4">--}}
                            {{--<label for="get-email" class="get-email">--}}
                                {{--<input type="checkbox" id="get-email">--}}
                                {{--<span class="get-email-check"></span>--}}
                                {{--<span class="col-4e font-weight-bold">می‌خواهم ایمیل‌های خبرنامه را دریافت کنم.</span>--}}
                            {{--</label>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row mx-0">--}}
                        {{--<input class="mr-auto green-btn" type="submit" value="ثبت">--}}
                    {{--</div>--}}
                {{--</form>--}}
                {{--<p class="line col-4e mt-3">تغییر رمز عبور</p>--}}
                {{--<form class="text-right w-100 edit-profile-form col-4e">--}}
                    {{--<div class="row mx-0">--}}
                        {{--<div class="col-12 col-md-4">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="old-password">رمز عبور فعلی</label>--}}
                                {{--<input type="password" class="form-control" id="old-password">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-12 col-md-4">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="new-password">رمز عبور جدید</label>--}}
                                {{--<input type="password" class="form-control" id="new-password">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-12 col-md-4">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="confirm-new-password">تکرار رمز جدید</label>--}}
                                {{--<input type="password" class="form-control" id="confirm-new-password">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row mx-0 mt-3">--}}
                        {{--<input class="mr-auto green-btn" type="submit" value="ثبت">--}}
                    {{--</div>--}}
                {{--</form>--}}
            </div>
        </div>
    </div>
</main>


@endsection



@section('js')

@endsection

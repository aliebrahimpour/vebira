@extends('layouts.app')

@section('content')
    <main>
        <div class="container">
            <div class="row">
                <div class="sidebar-right"></div>
                @include('layouts.userSidebar')

                <div id="edit-albums" class="col-md-10 dashboard p-4" data-visible="true">
                    <img class="top-img" src="<?= Url('images/dashboard.png') ?>" alt="">
                    <div class="row mt-4">
                        <div class="dashboard-top-item col-md-6 col-12 text-center">
                            <a id="new-album-btn" href="#new-album">ساخت آلبوم</a>
                        </div>
                    </div>
                </div>
                <div id="new-albums" class="col-md-10 dashboard p-4" data-visible="false">
                    <p class="line col-4e">ساخت آلبوم</p>
                        <form class="text-right w-100 edit-profile-form col-4e" action="{{route('upload.podcast')}}" method="post"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="row">
                            <div class="col-12 col-md-2">
                                <label for="img" class="change-profile-img">
                                    <input class="d-none" type="file" name="img" id="img">
                                    <img class="" src="<?= Url('images/Avatar.png') ?>" alt="">
                                    <span class="small col-4e">تغییر عکس</span>
                                </label>
                            </div>
                            <div class="col-12 col-md-10">
                                <div class="form-group">
                                    <label for="title">عنوان</label>
                                    <span class="require-filed">*</span>
                                    <input dir="ltr" name="title" class="form-control text-left" id="title">
                                </div>
                                <div class="form-group">
                                    <label for="description">توضیحات</label>
                                    <span class="require-filed">*</span>
                                    <textarea name="description" class="form-control" id="description" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="category">دسته‌بندی</label>
                                    <span class="require-filed">*</span>
                                    <select name="category_id" class="form-control">
                                        @php $SoundCategorys = \App\Models\SoundCategory::all() @endphp
                                        @foreach($SoundCategorys as $SoundCategory)
                                            <option value="{{$SoundCategory->id}}"
                                                    selected="selected">{{$SoundCategory->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{--<div class="form-group tags-form">--}}
                                    {{--<label for="tags">برچسب ها</label>--}}
                                    {{--<input class="form-control" type="text" name="tags" id="tags">--}}
                                    {{--<span class="add-tag-btn">افزودن</span>--}}
                                {{--</div>--}}
                            </div>
                            <div class="col-12 col-md-6 mt-auto">
                                <div class="form-group speaker-form">
                                    <label for="speaker">گویندان</label>
                                    <span class="require-filed">*</span>
                                    <input class="form-control" type="text" name="speakers" id="speakers">
                                    {{--<span class="add-tag-btn">افزودن</span>--}}
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mt-auto">
                                <div class="form-group speaker-form">
                                    <label for="speaker">تصویر</label>
                                    <span class="require-filed">*</span>
                                    <input type="file" class="form-control-file" name="picture" id="picture">
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mt-auto">
                                <div class="form-group speaker-form">
                                    <label for="speaker">پادکست</label>
                                    <span class="require-filed">*</span>
                                    <input type="file" class="form-control-file" name="podcast" id="podcast">
                                </div>
                            </div>
                        </div>
                        <div class="row mx-0 mt-3">
                            <input class="mr-auto green-btn" type="submit" value="ثبت">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

@endsection



@section('js')
    <script>
      $(document).ready(function(){
        $("#new-albums").fadeOut(0);
        $(".add-tag-btn").click(function(){
          var text = $(this).parent().find("#tags").val();
          var speaker_text = $(this).parent().find("#speaker").val();
          if(text != undefined){
            if(text != ""){
              var e_length = $(".tags-item").length;
              var data = "tag-item-"+(e_length+1);
              $("#tags-item").append("<p data-tag-name='"+data+"' class='tags-item'>"+text+"<span class='fa fa-trash-alt delete-tag'></span></p>");
              $(this).parent().find("#tags").val("");
            }
          }
          if(speaker_text != undefined){
            if(speaker_text != ""){
              var e_length = $(".speaker-item").length;
              var data = "speaker-item-"+(e_length+1);
              $("#speaker-item").append("<p data-tag-name='"+data+"' class='speaker-item'>"+speaker_text+"<span class='fa fa-trash-alt delete-tag'></span></p>");
              $(this).parent().find("#speaker").val("");
            }
          }
          $(".delete-tag").click(function(){
            $(this).parent().remove();
          });
        });
        $("#new-album-btn").click(function(){
          if($("#new-albums").attr("data-visible") == "false"){
            $("#new-albums").attr("data-visible", "true");
            $("#edit-albums").attr("data-visible", "false");
            $("#edit-albums").addClass("d-none");
            $("#new-albums").fadeOut(0);
            $("#new-albums").fadeIn();
            $("#new-albums").removeClass("d-none");
          }
        });

      });
    </script>

@endsection

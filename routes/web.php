<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'HomeController@search');


Route::group(['namespace' => 'Panel', 'middleware' => ['auth:web', 'checkAdmin'], 'prefix' => 'panel'], function () {
    Route::get('/', 'PanelController@index')->name('panel');

    //podcast
    Route::post('podcast/accept/{podcast_id}', 'SoundController@accept')->name('podcast.accept')->middleware('can:podcast');
    Route::post('podcast/unAccept/{podcast_id}', 'SoundController@unAccept')->name('podcast.unAccept')->middleware('can:podcast');
    Route::post('upload-image' , 'SoundController@uploadImageSubject');

    Route::resource('podcast', 'SoundController')->middleware('can:podcast');


    //podcast-category
    Route::resource('category', 'SoundCategoryController')->middleware('can:podcast')->names('panelCategory');


    //comment
    Route::post('comment/accept/{comment_id}', 'CommentController@accept')->name('comment.accept')->middleware('can:comment');
    Route::post('comment/unAccept/{comment_id}', 'CommentController@unAccept')->name('comment.unAccept')->middleware('can:comment');
    Route::resource('comment', 'CommentController')->middleware('can:comment');

    //user
    Route::post('user/accept/{user_id}', 'UserController@accept')->name('user.accept');
    Route::post('user/unAccept/{user_id}', 'UserController@unAccept')->name('user.unAccept');
    Route::resource('user', 'UserController');

    //admin
    Route::resource('admin/role', 'RoleController')->middleware('can:admin-role');
    Route::resource('admin/permission', 'PermissionController')->middleware('can:admin-role');
    Route::resource('admin', 'AdminController', ['parameters' => ['role' => 'user']])->middleware('can:admin-role');
    Route::resource('static-page', 'StaticPageController');

    //menu
    Route::get('menu', 'SettingController@menu')->name('menu.index');
    Route::post('menu', 'SettingController@addToMenu')->name('menu.store');
    Route::delete('menu/{menu}', 'SettingController@removeToMenu')->name('menu.destroy');

    //setting
    Route::get('public', 'SettingController@publicSetting')->name('public.index');
    Route::post('public', 'SettingController@updateSetting')->name('public.store');

    //slider
    Route::resource('slider', 'SliderController');
    Route::resource('sliderPic', 'SliderPicController');

});


Route::group(['namespace' => 'Panel', 'middleware' => ['auth:web'], 'prefix' => 'user-panel'], function () {
    Route::get('/', 'PanelController@UserIndex')->name('user-panel.index');
    Route::get('profile', 'PanelController@UserProfile')->name('user-panel.profile');
    Route::patch('profile', 'PanelController@updateProfile')->name('profile.update');
    Route::get('upload', 'PanelController@UserUpload')->name('user-panel.upload');
    Route::post('upload', 'PanelController@UserUploadpodcast')->name('upload.podcast');
    Route::delete('upload', 'PanelController@UserDeletepodcast')->name('delete.podcast');
    Route::get('buy', 'PanelController@UserBuy')->name('user-panel.buy');

});

Auth::routes();

Route::get('podcast/{podcast_id}', 'ProductController@index');


Route::get('category/{category_id}', 'CategoryController@index')->name('category.index');

Route::resource('cart', 'CartController');
Route::group(['middleware' => ['auth:web']], function () {
    Route::get('podcast/{podcast_id}/addtocard', 'ProductController@addtocard');
    Route::get('podcast/{podcast_id}/removetocard', 'ProductController@removetocard');
});

Route::post('/product/payment', 'PaymentController@payment');
Route::get('/product/callback', 'PaymentController@callback');


Route::get('{page}', 'HomeController@staticPages');


//Route::resource('','CategoryController');
//Route::get('/home', 'HomeController@index')->name('home');
